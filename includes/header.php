<?php session_start() ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="public/assets/plugins/fontawesome-free/css/all.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="public/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="public/assets/dist/css/adminlte.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
    <!-- jQuery -->
    <script src="public/assets/plugins/jquery/jquery.min.js"></script>


    <!-- mystyle CSS -->
    <link href="./public/assets/css/mystyle.css" rel="stylesheet">

    <!-- icon CSS -->
    <link href="./public/assets/css/font-awesome.min.css" rel="stylesheet">

    <title>Booking</title>

    <style>
      /* Paste this css to your style sheet file or under head tag */
      /* This only works with JavaScript, 
      if it's not present, don't show loader */
      .no-js #loader { display: none;  }
      .js #loader { display: block; position: absolute; left: 100px; top: 0; }
      .se-pre-con {
          position: fixed;
          left: 0px;
          top: 0px;
          width: 100%;
          height: 100%;
          z-index: 9999;
          background: url("public/assets/images/Preloader_11.gif") center no-repeat #fff;
      }
    </style>

    <!-- Animation Text -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <!-- Start WOWSlider.com HEAD section -->
    <link rel="stylesheet" type="text/css" href="public/assets/silde/style.css" />
    <!-- <script type="text/javascript" src="engine1/jquery.js"></script> -->
    <!-- End WOWSlider.com HEAD section -->

</head>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>

<!-- Animation Text -->
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

<script>
    //paste this code under the head tag or in a separate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeToggle("slow");
    AOS.init();
	});
</script>

<body">
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
<div class="se-pre-con"></div>
  <div class="container-fluid">
    <a class="navbar-brand" href="index.php?view=home"><img src="./public/assets/images/logo.png"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php?view=home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php?view=room">Rooms List</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php?view=contact">Contact Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php?view=about">About Us</a>
        </li>
        <?php
        if(!isset($_SESSION['guest_login']))
        { ?>
          <li class="nav-item">
            <a class="nav-link" href="index.php?view=signup">Register</a>
          </li>
          <?php } ?>
          
          <?php
          if(!isset($_SESSION['guest_login']))
          { ?>
            <li class="nav-item">
              <a class="nav-link" href="index.php?view=login">Login</a>
            </li>
            <?php } ?>

            <?php
            if(isset($_SESSION['guest_login']))
            { ?>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                My Account
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#" onclick="signout()">Signout Account</a></li>
                <li><a class="dropdown-item" href="index.php?view=my_account">My Account</a></li>
              </ul>
            </li>
            <?php } ?>
      </ul>
      <!-- <div class="d-flex">
        <button onclick="booking()" class="form-control btn btn-primary">Book Now</button>
      </div> -->
    </div>
  </div>
</nav>

<script>
  // Signout 
  const signout = () => {
    // Guest Signout
    $.ajax({
        url: "models/guestSession.php",
        type: "GET",
        data: { signout: 'signout' },
        success: function(res)
        {
          let logoutMessage = JSON.parse(res);
          if(logoutMessage == 'logout')
          {
            window.location.replace = "index.php?view=home";
            window.location.reload();
          }
        }
    });
  }

  const  checkGuestSession = (() => {
    // Check session
    $.ajax({
        url: "models/guestSession.php",
        type: "GET",
        data: { checkSession: 'checkSession' },
        success: function(res)
        {
          let guestSession = JSON.parse(res);
          if(!guestSession == 1)
          {
            swal("Please login!", "You can't use service reservation", "warning");
          }
          else
          {
            window.location.href = "index.php?view=room";
          }
        }
      });
  })

  // Booking Detal 
  function bookingDetail(room_id)
  {
    window.location.href = `index.php?view=booking_detail&id=${room_id}`;
  }

  function booking()
  {
    checkGuestSession()
  }
</script>