<?php
include("../Config.php");
include("../models/BaseModel.php");
include("../models/Floor.php");
session_start();

$floorModel = new Floor();
$currentDate = date("Y-m-d h:i:sa");
$floorModel->setName($_POST['name']);
$floorModel->setDescription($_POST['description']);
$floorModel->setCreatedAt($currentDate);
$floorModel->setCreatedBy($_SESSION['user_login']);
$floorModel->setUpdatedAt($currentDate);
$floorModel->setUpdatedBy($_SESSION['user_login']);

$floorObject = array(
    "name" => $floorModel->getName(),
    "description" => $floorModel->getDescription(),
    "created_at" => $floorModel->getCreatedAt(),
    "created_by" => $floorModel->getCreatedBy(),
    "updated_at" => $floorModel->getUpdatedAt(),
    "updated_by" => $floorModel->getUpdatedBy()
);

$floorModel->createFloor($floorObject);