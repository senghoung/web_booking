<?php
include("../Config.php");
include("../models/BaseModel.php");
include("../models/User.php");
session_start();

$userModel = new User();
$currentDate = date("Y-m-d h:i:sa");

$userModel->setId($_POST['user_id']);
$userModel->setUsername($_POST['username']);
$userModel->setEmail($_POST['email']);
$userModel->setPassword(password_hash($_POST['password'], PASSWORD_DEFAULT));
$userModel->setRole($_POST['role_id']);
$userModel->setUpdatedAt($currentDate);
$userModel->setUpdatedBy($_SESSION['user_login']);
$userObject = array();

$userObject = array(
    "id" => $userModel->getId(),
    "username" => $userModel->getUsername(),
    "email" => $userModel->getEmail(),
    "password" => $userModel->getPassword(),
    "role_id" => $userModel->getRole(),
    "updated_at" => $userModel->getUpdatedAt(),
    "updated_by" => $userModel->getUpdatedBy()
);

$userModel->updateUser($userObject);