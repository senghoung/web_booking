<?php
include("../Config.php");
include("../models/BaseModel.php");
include("../models/Role.php");
session_start();

$roleModel = new Role();
$currentDate = date("Y-m-d h:i:sa");
$roleModel->setId($_POST['role_id']);
$roleModel->setName($_POST['name']);
$roleModel->setDescription($_POST['description']);
$roleModel->setUpdatedAt($currentDate);
$roleModel->setUpdatedBy($_SESSION['user_login']);

$roleObject = array(
    "id" => $roleModel->getId(),
    "name" => $roleModel->getName(),
    "description" => $roleModel->getDescription(),
    "updated_at" => $roleModel->getUpdatedAt(),
    "updated_by" => $roleModel->getUpdatedBy()
);

$roleModel->updateRole($roleObject);