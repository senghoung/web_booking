<?php
include("../Config.php");
include("../models/BaseModel.php");
include("../models/Room.php");
session_start();

$roomModel = new Room();
$currentDate = date("Y-m-d h:i:sa");

$uploads_dir = "../public/assets/images/uploads/";
$tmp_name = $_FILES["fileUpload"]["tmp_name"];
$name = basename($_FILES["fileUpload"]["name"]);
move_uploaded_file($tmp_name, "$uploads_dir/$name");

$roomModel->setFloorId($_POST['floor']);
$roomModel->setName($_POST['name']);
$roomModel->setRoomNumber($_POST['room_number']);
$roomModel->setPrice($_POST['price']);
$roomModel->setPhoto($name);
$roomModel->setDescription($_POST['description']);
$roomModel->setCreatedAt($currentDate);
$roomModel->setCreatedBy($_SESSION['user_login']);
$roomModel->setUpdatedAt($currentDate);
$roomModel->setUpdatedBy($_SESSION['user_login']);

$roomObject = array(
    "floor_id" => $roomModel->getFloorId(),
    "name" => $roomModel->getName(),
    "room_number" => $roomModel->getRoomNumber(),
    "price" => $roomModel->getPrice(),
    "photo" => $roomModel->getPhoto(),
    "status" => 1,
    "description" => $roomModel->getDescription(),
    "created_at" => $roomModel->getCreatedAt(),
    "created_by" => $roomModel->getCreatedBy(),
    "updated_at" => $roomModel->getUpdatedAt(),
    "updated_by" => $roomModel->getUpdatedBy()
);

$roomModel->createRoom($roomObject);