<?php
include("../Config.php");
include("../models/BaseModel.php");
include("../models/Guest.php");

$guestModel = new Guest();

$guestModel->setFirstName($_POST['first_name']);
$guestModel->setLastName($_POST['last_name']);
$guestModel->setPhone($_POST['phone']);
$guestModel->setEmail($_POST['email']);
$guestModel->setPassword(password_hash($_POST['password'], PASSWORD_DEFAULT));

$guestSignupObject = array(
    "first_name" => $guestModel->getFirstName(),
    "last_name" => $guestModel->getLastName(),
    "phone" => $guestModel->getPhone(),
    "email" => $guestModel->getEmail(),
    "password" => $guestModel->getPassword()
);

$guestModel->guestCreate($guestSignupObject);

