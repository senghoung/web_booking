<?php
include("../Config.php");
include("../models/BaseModel.php");
include("../models/Booking.php");
session_start();

$bookModel = new Booking();

$bookModel->setGuestId($_SESSION['guest_login']);
$bookModel->setRoomId($_POST['room_id']);
$bookModel->setFromDate($_POST['from_date']);
$bookModel->setToDate($_POST['to_date']);
$bookModel->setPrice($_POST['total_price']);
$bookModel->setPerson($_POST['person']);
$bookModel->setChild($_POST['child']);
$bookModel->setStatus(1);

$bookingObject = array(
    "guest_id" => $bookModel->getGuestId(),
    "room_id" => $bookModel->getRoomId(),
    "from_date" => $bookModel->getFromDate(),
    "to_date" => $bookModel->getToDate(),
    "total_price" => $bookModel->getPrice(),
    "person" => $bookModel->getPerson(),
    "child" => $bookModel->getChild(),
    "status" => $bookModel->getStatus()
);

$bookModel->createBooking($bookingObject);