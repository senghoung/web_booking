<?php 

class BaseModel {

    protected $con;

    function __construct()
    {
        $config = new Config();
        $this->con = $config->dbConnection();
    }

    function querySql($sql)
    {
        $result = $this->con->prepare($sql);
        $result->execute();
        return $result->fetchAll();
    }

    function findAll($sql)
    {
        $result = $this->con->prepare($sql);
        $result->execute();
        return $result->fetchAll();
    }

    function findById($tableName, $field, $id)
    {
        try
        {
            $result = $this->con->query("SELECT * FROM $tableName WHERE $field = $id");
            return $result->fetch();
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    function create($sql, array $data)
    {
        try
        {
            $result = $this->con->prepare($sql);
            $result->execute($data);
            echo json_encode($result);
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    function update($sql, array $data)
    {
        $result = $this->con->prepare($sql);
        $result->execute($data);
        echo json_encode($result);
    }

    function destroy($tableName, $condition)
    {
        try
        {
            return $this->con->query("DELETE FROM $tableName WHERE $condition");
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}