<?php

class User extends BaseModel {
    private $id;
    private $username;
    private $password;
    private $email;
    private $role_id;
    private $created_at;
    private $created_by;
    private $updated_at;
    private $updated_by;
    protected $tableName = "users";

    function getId()
    {
        return $this->id;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function getUsername()
    {
        return $this->username;
    }

    function setUsername($username)
    {
        $this->username = $username;
    }

    function getPassword()
    {
        return $this->password;
    }

    function setPassword($password)
    {
        return $this->password = $password;
    }

    function getEmail()
    {
        return $this->email;
    }

    function setEmail($email)
    {
        $this->email = $email;
    }

    function getRole()
    {
        return $this->role_id;
    }

    function setRole($role_id)
    {
        $this->role_id = $role_id;
    }

    function getCreatedAt()
    {
        return $this->created_at;
    }

    function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    function getCreatedBy()
    {
        return $this->created_by;
    }

    function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    function getUpdatedAt()
    {
        return $this->updated_at;
    }

    function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function getUpdatedBy()
    {
        return $this->updated_by;
    }

    function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;
    }

    function getUserAll()
    {
       return $this->findAll("SELECT
        tbl_user.id,
        tbl_user.username,
        tbl_user.email,
        tbl_user.username,
        tbl_user.created_at,
        tbl_user.created_at,
        tbl_user.updated_at,
        tbl_role.name AS role_name,
        (SELECT users.username FROM users WHERE users.id = tbl_user.created_by) AS createdby,
        (SELECT users.username FROM users WHERE users.id = tbl_user.updated_by) AS updatedby
       FROM `users` AS tbl_user 
       INNER JOIN `roles` tbl_role
       ON tbl_user.role_id = tbl_role.id");
    }

    function getUserById($id)
    {
        $result = $this->findById($this->tableName, "id", $id);
        return $result;
    }

    function deleteUserById($id)
    {
        $this->destroy($this->tableName, "id = $id");
        echo json_encode($id);
    }

    function userCreate(array $data)
    {
        $sql = "INSERT INTO  $this->tableName (username, email, password, role_id, created_at, created_by, updated_at, updated_by) VALUES(:username, :email, :password, :role_id, :created_at, :created_by, :updated_at, :updated_by)";
        $result = $this->create($sql, $data);
        echo json_encode($result);
    }

    function updateUser(array $data)
    {
        $sql = null;
        if($data['password'] != '')
        {
            $this->$sql = "UPDATE $this->tableName SET username = :username, email = :email, password = :password, role_id = :role_id, updated_at = :updated_at, updated_by = :updated_by WHERE id = :id";
        }
        else
        {
            $this->$sql = "UPDATE $this->tableName SET username = :username, email = :email, role_id = :role_id, updated_at = :updated_at, updated_by = :updated_by WHERE id = :id";
        }
        $result = $this->update($this->$sql, $data);
        echo json_encode($result);
    }

    function userLogin(array $data)
    {
        try
        {
            $sql = "SELECT
            tbl_user.password
            FROM `users` AS tbl_user
            WHERE tbl_user.email = '".$data['email']."'";
            $hashPassword = $this->querySql($sql);

            foreach ($hashPassword as $value) {
                $hashPassword = $value['password'];
            }

            if(!$hashPassword == [] )
            {
                // Verify the hash against the password entered
                $verify = password_verify($data['password'], $hashPassword);

                // Print the result depending if they match 
                if ($verify == 1) 
                { 
                    session_start();
                    $sql = "SELECT
                    tbl_user.id
                    FROM `users` AS tbl_user
                    WHERE tbl_user.password = '$hashPassword' ";
                    $getUserId = $this->querySql($sql);

                    foreach ($getUserId as $value) {
                        $getUserId = $value['id'];
                    }

                    $_SESSION["user_login"] = $getUserId;
                    echo json_encode("success");
                } 
                else 
                { 
                    echo json_encode("error");
                }
            }
            else
            {
                echo json_encode("error");
            }
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}