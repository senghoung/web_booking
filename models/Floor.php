<?php

class Floor  extends BaseModel {
    private $id;
    private $name;
    private $description;
    private $created_at;
    private $created_by;
    private $updated_at;
    private $updated_by;
    protected $tableName = "floors";

    function getId()
    {
        return $this->id;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function getName()
    {
        return $this->name;
    }

    function setName($name)
    {
        $this->name = $name;
    }

    function getDescription()
    {
        return $this->description;
    }

    function setDescription($description)
    {
        $this->description = $description;
    }

    function getCreatedAt()
    {
        return $this->created_at;
    }

    function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    function getCreatedBy()
    {
        return $this->created_by;
    }

    function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    function getUpdatedAt()
    {
        return $this->updated_at;
    }

    function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function getUpdatedBy()
    {
        return $this->updated_by;
    }

    function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;
    }

    function getFloorAll()
    {
        $sql = "SELECT
        tbl_floor.id,
        tbl_floor.name,
        tbl_floor.description,
        (SELECT users.username FROM users WHERE users.id = tbl_floor.created_by) AS createdby,
        (SELECT users.username FROM users WHERE users.id = tbl_floor.updated_by) AS updatedby,
        tbl_floor.created_at,
        tbl_floor.updated_at
        FROM `floors` AS tbl_floor";

        $result =  $this->findAll($sql);
        return $result;
    }

    function getFloorById($id)
    {
        $result = $this->findById($this->tableName, "id", $id);
        return $result;
    }

    function createFloor(array $data)
    {
        $sql = "INSERT INTO  $this->tableName (name, description, created_at, created_by, updated_at, updated_by) VALUES(:name, :description, :created_at, :created_by, :updated_at, :updated_by)";
        $result = $this->create($sql, $data);
        echo json_encode($result);
    }

    function updateFloor(array $data)
    {
        $sql = "UPDATE $this->tableName SET name = :name, description = :description, updated_at = :updated_at, updated_by = :updated_by WHERE id = :id";
        $result = $this->update($sql, $data);
        echo json_encode($result);
    }

    function deleteFloorById($id)
    {
        $this->destroy($this->tableName, "id = $id");
        echo json_encode($id);
    }
}