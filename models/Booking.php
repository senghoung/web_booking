<?php

class Booking extends BaseModel {
    private $id;
    private $guest_id;
    private $room_id;
    private $from_date;
    private $to_date;
    private $price;
    private $tax;
    private $person;
    private $child;
    private $status;
    private $created_at;
    private $created_by;
    private $updated_at;
    private $updated_by;
    protected $tableName = "booking";

    function getId()
    {
        return $this->id;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function getGuestId()
    {
        return $this->guest_id;
    }

    function setGuestId($guest_id)
    {
        $this->guest_id = $guest_id;
    }

    function getRoomId()
    {
        return $this->room_id;
    }

    function setRoomId($room_id)
    {
        $this->room_id = $room_id;
    }

    function getFromDate()
    {
        return $this->from_date;
    }

    function setFromDate($from_date)
    {
        $this->from_date = $from_date;
    }

    function getToDate()
    {
        return $this->to_date;
    }

    function setToDate($to_date)
    {
        $this->to_date = $to_date;
    }

    function getPrice()
    {
        return $this->price;
    }

    function setPrice($price)
    {
        $this->price = $price;
    }

    function getTax()
    {
        return $this->tax;
    }

    function setTax($tax)
    {
        $this->tax = $tax;
    }

    function getPerson()
    {
        return $this->person;
    }

    function setPerson($person)
    {
        $this->person = $person;
    }

    function getChild()
    {
        return $this->child;
    }

    function setChild($child)
    {
        $this->child = $child;
    }

    function getStatus()
    {
        return $this->status;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }

    function getCreatedAt()
    {
        return $this->created_at;
    }

    function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    function getCreatedBy()
    {
        return $this->created_by;
    }

    function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    function getUpdatedAt()
    {
        return $this->updated_at;
    }

    function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function getUpdatedBy()
    {
        return $this->updated_by;
    }

    function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;
    }

    function getBookingAll()
    {
        $sql = "SELECT
        tbl_booking.id,
        tbl_booking.from_date,
        tbl_booking.to_date,
        tbl_booking.tax,
        tbl_booking.person,
        tbl_booking.child,
        IF(tbl_booking.`status` = 1, 'Paid', 'Pendding') AS `status`,
        tbl_room.name AS room_name,
        CONCAT(tbl_guest.first_name, ' ', tbl_guest.last_name) AS guest_name,
        tbl_booking.total_price
        FROM booking AS tbl_booking 
        INNER JOIN guests AS tbl_guest 
        ON tbl_booking.guest_id = tbl_guest.id
        INNER JOIN rooms AS tbl_room 
        ON tbl_room.id = tbl_booking.room_id ORDER BY id DESC";

        $result =  $this->findAll($sql);
        return $result;
    }

    function getBookingByRoom()
    {
        $sql = "SELECT
        tbl_booking.id,
        tbl_booking.from_date,
        tbl_booking.to_date,
        tbl_booking.tax,
        SUM(tbl_booking.person) AS person,
        SUM(tbl_booking.child) AS child,
        IF(tbl_booking.`status` = 1, 'Paid', 'Pendding') AS `status`,
        tbl_room.name AS room_name,
        CONCAT(tbl_guest.first_name, ' ', tbl_guest.last_name) AS guest_name,
        tbl_booking.total_price
        FROM booking AS tbl_booking 
        INNER JOIN guests AS tbl_guest 
        ON tbl_booking.guest_id = tbl_guest.id
        INNER JOIN rooms AS tbl_room 
        ON tbl_room.id = tbl_booking.room_id GROUP BY tbl_booking.room_id";

        $result =  $this->findAll($sql);
        return $result;
    }

    function createBooking(array $data)
    {
        $sql = "INSERT INTO  $this->tableName (guest_id, room_id, from_date, to_date, total_price, person, child, status) VALUES(:guest_id, :room_id, :from_date, :to_date, :total_price, :person, :child, :status)";
        $result =  $this->create($sql, $data);
        echo json_encode($result);
    }
}