<?php
include("../Config.php");
include("../models/BaseModel.php");
include("../models/Role.php");
session_start();

$roleModel = new Role();
$currentDate = date("Y-m-d h:i:sa");
$roleModel->setName($_POST['name']);
$roleModel->setDescription($_POST['description']);
$roleModel->setCreatedAt($currentDate);
$roleModel->setCreatedBy($_SESSION['user_login']);
$roleModel->setUpdatedAt($currentDate);
$roleModel->setUpdatedBy($_SESSION['user_login']);

$roleObject = array(
    "name" => $roleModel->getName(),
    "description" => $roleModel->getDescription(),
    "created_at" => $roleModel->getCreatedAt(),
    "created_by" => $roleModel->getCreatedBy(),
    "updated_at" => $roleModel->getUpdatedAt(),
    "updated_by" => $roleModel->getUpdatedBy()
);

$roleModel->createRole($roleObject);