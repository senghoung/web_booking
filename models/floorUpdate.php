<?php
include("../Config.php");
include("../models/BaseModel.php");
include("../models/Floor.php");
session_start();

$floorModel = new Floor();
$currentDate = date("Y-m-d h:i:sa");
$floorModel->setId($_POST['floor_id']);
$floorModel->setName($_POST['name']);
$floorModel->setDescription($_POST['description']);
$floorModel->setUpdatedAt($currentDate);
$floorModel->setUpdatedBy($_SESSION['user_login']);

$floorObject = array(
    "id" => $floorModel->getId(),
    "name" => $floorModel->getName(),
    "description" => $floorModel->getDescription(),
    "updated_at" => $floorModel->getUpdatedAt(),
    "updated_by" => $floorModel->getUpdatedBy()
);

$floorModel->updateFloor($floorObject);