<?php
include("../Config.php");
include("../models/BaseModel.php");
include("../models/Guest.php");
session_start();

$guestModel = new Guest();
$currentDate = date("Y-m-d h:i:sa");

$guestModel->setFirstName($_POST['first_name']);
$guestModel->setLastName($_POST['last_name']);
$guestModel->setPassword(password_hash($_POST['password'], PASSWORD_DEFAULT));
$guestModel->setEmail($_POST['email']);
$guestModel->setPhone($_POST['phone']);

$guestObject = array(
    "first_name" => $guestModel->getFirstName(),
    "last_name" => $guestModel->getLastName(),
    "email" => $guestModel->getEmail(),
    "phone" => $guestModel->getPhone(),
    "password" => $guestModel->getPassword()
);

$guestModel->guestCreate($guestObject);