<?php
include("../Config.php");
include("../models/BaseModel.php");
include("../models/User.php");
session_start();

$userModel = new User();
$currentDate = date("Y-m-d h:i:sa");
$userModel->setUsername($_POST['username']);
$userModel->setEmail($_POST['email']);
$userModel->setPassword(password_hash($_POST['password'], PASSWORD_DEFAULT));
$userModel->setRole($_POST['role_id']);
$userModel->setCreatedAt($currentDate);
$userModel->setCreatedBy($_SESSION['user_login']);
$userModel->setUpdatedAt($currentDate);
$userModel->setUpdatedBy($_SESSION['user_login']);

$userObject = array(
    "username" => $userModel->getUsername(),
    "email" => $userModel->getEmail(),
    "password" => $userModel->getPassword(),
    "role_id" => $userModel->getRole(),
    "created_at" => $userModel->getCreatedAt(),
    "created_by" => $userModel->getCreatedBy(),
    "updated_at" => $userModel->getUpdatedAt(),
    "updated_by" => $userModel->getUpdatedBy()
);

$userModel->userCreate($userObject);