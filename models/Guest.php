<?php

class Guest extends BaseModel {
    private $id;
    private $first_name;
    private $last_name;
    private $email;
    private $phone;
    private $password;
    protected $tableName = "guests";

    function getId()
    {
        return $this->id;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function getFirstName()
    {
        return $this->first_name;
    }

    function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    function getLastName()
    {
        return $this->last_name;
    }

    function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    function getEmail()
    {
        return $this->email;
    }

    function setEmail($email)
    {
        $this->email = $email;
    }

    function getPhone()
    {
        return $this->phone;
    }

    function setPhone($phone)
    {
        $this->phone = $phone;
    }

    function getPassword()
    {
        return $this->password;
    }

    function setPassword($password)
    {
        $this->password = $password;
    }

    function getGuestAll()
    {
        $sql = "SELECT * FROM guests ORDER BY id DESC";

        $result =  $this->findAll($sql);
        return $result;
    }

    function getGuestById($id)
    {
        $result = $this->findById($this->tableName, "id", $id);
        return $result;
    }

    function deleteGuestById($id)
    {
        $this->destroy($this->tableName, "id = $id");
        echo json_encode($id);
    }

    function guestCreate(array $data)
    {
        $sql = "INSERT INTO  $this->tableName (first_name, last_name, email, phone, password) VALUES(:first_name, :last_name, :email, :phone, :password)";
        $result = $this->create($sql, $data);
        echo json_encode($result);
    }

    function guestUpdate(array $data)
    {
        $sql = null;
        if($data['password'] != '')
        {
            $this->$sql = "UPDATE $this->tableName SET first_name = :first_name, last_name = :last_name, email = :email, phone = :phone, password = :password WHERE id = :id";
        }
        else
        {
            $this->$sql = "UPDATE $this->tableName SET first_name = :first_name, last_name = :last_name, email = :email, phone = :phone WHERE id = :id";
        }
        $result = $this->update($this->$sql, $data);
        echo json_encode($result);
    }

    function guestLogin($data)
    {
        try
        {
            // Check guest login from guest
            $sql = "SELECT
            tbl_guest.password
            FROM `guests` AS tbl_guest
            WHERE tbl_guest.email = '".$data['email']."'";
            $hashPassword = $this->querySql($sql);

            foreach ($hashPassword as $value) {
                $hashPassword = $value['password'];
            }

            if(!$hashPassword == [] )
            {
                // Verify the hash against the password entered
                $verify = password_verify($data['password'], $hashPassword);

                // Print the result depending if they match 
                if ($verify == 1) 
                { 
                    session_start();
                    $sql = "SELECT
                    tbl_guest.id
                    FROM `guests` AS tbl_guest
                    WHERE tbl_guest.password = '$hashPassword' ";
                    $getGuestId = $this->querySql($sql);

                    foreach ($getGuestId as $value) {
                        $getGuestId = $value['id'];
                    }

                    $_SESSION["guest_login"] = $getGuestId;
                    echo json_encode("success");
                } 
                else 
                { 
                    echo json_encode("error");
                }
            }
            else
            {
                echo json_encode("error");
            }
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}