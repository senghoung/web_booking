<?php
include("../Config.php");
include("../models/BaseModel.php");
include("../models/User.php");

if(isset($_GET['logout']))
{
    session_start();
    unset($_SESSION['user_login']);
    echo json_encode("logout");
}
else
{
    $userModel = new User();
    $userModel->setEmail($_POST['email']);
    $userModel->setPassword($_POST['password']);

    $userObject = array(
        'email' => $userModel->getEmail(),
        'password' => $userModel->getPassword()
    );

    $userModel->userLogin($userObject);
}