<?php
include("../Config.php");
include("../models/BaseModel.php");
include("../models/Guest.php");

$guestModel = new Guest();

$guestModel->setEmail($_POST['email']);
$guestModel->setPassword($_POST['password']);

$guestLoginObject = array(
    "email" => $guestModel->getEmail(),
    "password" => $guestModel->getPassword(),
);

$guestModel->guestLogin($guestLoginObject);

