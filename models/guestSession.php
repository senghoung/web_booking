<?php
include("../Config.php");
include("../models/BaseModel.php");
session_start();

if(isset($_GET['signout']))
{
    unset($_SESSION['guest_login']);
    echo json_encode('logout');
}
else
{
    if(isset($_GET['checkSession']))
    {
        if(!isset($_SESSION['guest_login']))
        {
            echo json_encode(0);
        }
        else
        {
                echo json_encode(1);
        }
    }
}