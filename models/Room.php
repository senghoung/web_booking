<?php

class Room extends BaseModel {
    private $id;
    private $name;
    private $room_number;
    private $floor_id;
    private $price;
    private $photo;
    private $status;
    private $description;
    private $created_at;
    private $created_by;
    private $updated_at;
    private $updated_by;
    protected $tableName = "rooms";

    function getId()
    {
        return $this->id;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function getRoomNumber()
    {
        return $this->room_number;
    }

    function setRoomNumber($room_number)
    {
        $this->room_number = $room_number;
    }

    function getFloorId()
    {
        return $this->floor_id;
    }

    function setFloorId($floor_id)
    {
        $this->floor_id = $floor_id;
    }

    function getPrice()
    {
        return $this->price;
    }

    function setPrice($price)
    {
        $this->price = $price;
    }

    function getStatus()
    {
        return $this->status;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }

    function getPhoto()
    {
        return $this->photo;
    }

    function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    function getName()
    {
        return $this->name;
    }

    function setName($name)
    {
        $this->name = $name;
    }

    function getDescription()
    {
        return $this->description;
    }

    function setDescription($description)
    {
        $this->description = $description;
    }

    function getCreatedAt()
    {
        return $this->created_at;
    }

    function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    function getCreatedBy()
    {
        return $this->created_by;
    }

    function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    function getUpdatedAt()
    {
        return $this->updated_at;
    }

    function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function getUpdatedBy()
    {
        return $this->updated_by;
    }

    function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;
    }

    function getRoomAll()
    {
        $sql = "SELECT
        tbl_room.id,
        tbl_room.name,
        tbl_room.room_number,
        tbl_room.floor_id,
        tbl_room.price,
        tbl_room.photo,
        tbl_floor.name AS floor_name,
        tbl_room.description,
        (SELECT users.username FROM users WHERE users.id = tbl_room.created_by) AS createdby,
        (SELECT users.username FROM users WHERE users.id = tbl_room.updated_by) AS updatedby,
        tbl_room.created_at,
        tbl_room.updated_at
        FROM `rooms` AS tbl_room
        LEFT JOIN `floors` AS tbl_floor ON tbl_room.floor_id = tbl_floor.id ORDER BY name DESC";

        $result =  $this->findAll($sql);
        return $result;
    }

    function getRoomById($id)
    {
        $result = $this->findById($this->tableName, "id", $id);
        return $result;
    }

    function createRoom(array $data)
    {
        $sql = "INSERT INTO  $this->tableName (name, room_number, floor_id, price, photo, description, status, created_at, created_by, updated_at, updated_by) VALUES(:name, :room_number, :floor_id, :price, :photo, :description, :status, :created_at, :created_by, :updated_at, :updated_by)";
        $result = $this->create($sql, $data);
        echo json_encode($result);
    }

    function updateRoom(array $data)
    {
        $sql = null;
        if(isset($data['photo']))
        {
            $this->$sql = "UPDATE $this->tableName SET name = :name, room_number = :room_number, floor_id = :floor_id, price = :price,  photo = :photo, description = :description, status = :status, updated_at = :updated_at, updated_by = :updated_by WHERE id = :id";
        }
        else
        {
            $this->$sql = "UPDATE $this->tableName SET name = :name, room_number = :room_number, floor_id = :floor_id, price = :price, description = :description, status = :status, updated_at = :updated_at, updated_by = :updated_by WHERE id = :id";
        }
        $result = $this->update($this->$sql, $data);
        echo json_encode($result);
    }

    function deleteRoomById($id)
    {
        $this->destroy($this->tableName, "id = $id");
        echo json_encode($id);
    }
}