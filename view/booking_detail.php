<?php
include("models/Room.php");

$roomModel = new Room();
$roomModel->setPhoto($roomModel->getRoomById($_GET['id'])['photo']);
$roomModel->setDescription($roomModel->getRoomById($_GET['id'])['description']);
$roomModel->setPrice($roomModel->getRoomById($_GET['id'])['price']);
$roomModel->setName($roomModel->getRoomById($_GET['id'])['name']);
$roomModel->setStatus($roomModel->getRoomById($_GET['id'])['status']);

?>
<style>
table td {
    border: 1px solid #64cedb;
    padding: 10px;
    font-family: -webkit-pictograph;
    font-size: 15px;
    color: #6c757d;
}
</style>
<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
    <div class="ws_images"><ul>
            <li><a href="http://wowslider.net">
                <img src="public/assets/silde/images/bg_login.jpg" alt="responsive slider" title="" id="wows1_0"/></a></li>
            <li>
                <img src="public/assets/silde/images/bg.jpg" alt="" title="" id="wows1_1"/></li>
        </ul></div>
        <div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">css image slider</a> by WOWSlider.com v9.0</div>
    <div class="ws_shadow"></div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div 
            data-aos="fade-down"
            data-aos-delay="10"
            data-aos-duration="2000">
                <h1 class="text-center home-text">
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                &nbsp;&nbsp;
                    Welcome to Star Hotel
                &nbsp;&nbsp;
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                </h1>
                <h3 class="text-center home-text">Booking Detail</h3>
            </div>
        </div>
    </div>
    <!-- Contact -->
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-3">
                    <img class="card-img-top" style="max-height: 430px;" src="public/assets/images/uploads/<?= $roomModel->getPhoto() ?>" alt="Card image cap">
                    <div class="card-body">
                        <p>
                            <span style="font-size: 16px;"> Room Name: <?= $roomModel->getName() ?> </span> 
                        </p>
                        <p>
                            <span style="font-size: 16px;"> Price: </span> 
                            <span style="color: red;font-size: 20px;"> $<?= $roomModel->getPrice() ?> </span> 
                        </p>
                        <p>
                            <span style="font-size: 16px;"> Status: <?php  if($roomModel->getStatus() == 1) echo "Available" ?></span> 
                        </p>
                        <p style="font-size: 15px;" class="card-text">
                            <?= $roomModel->getDescription() ?>
                        </p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                <form id="booking_checkout" method="POST">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="mb-2">Check-in <small class="text-danger">*</small> &nbsp;&nbsp;<i class="fa fa-calendar" aria-hidden="true"></i></label>
                                    <input type="date" name="checkin" id="checkin" class="form-control" placeholder="Checkin Date">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="mb-2">Check-out <small class="text-danger">*</small> &nbsp;&nbsp;<i class="fa fa-calendar" aria-hidden="true"></i></label>
                                    <input type="date" name="checkout" id="checkout" class="form-control" placeholder="Checkin Date">
                                    <input type="hidden" name="price" id="price" class="form-control" value="<?= $roomModel->getPrice() ?>">
                                    <input type="hidden" name="room_name" id="room_name" class="form-control" value="<?= $roomModel->getName() ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="mb-2">Person <small class="text-danger">*</small> &nbsp;&nbsp;<i class="fa fa-user" aria-hidden="true"></i></label>
                                    <select class="form-control" name="person" id="person">
                                        <option value="0">Select Person</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="mb-2">Children <small class="text-danger">*</small> &nbsp;&nbsp;<i class="fa fa-child" aria-hidden="true"></i></label>
                                    <select class="form-control" name="child" id="child">
                                        <option value="0">Select Child</option>
                                        <option value="1">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="submit" class="form-control btn btn-primary" value="Book Now">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="bookModal" tabindex="-1" role="dialog" aria-labelledby="bookModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"> Check-out</h5>
      </div>
      <div class="modal-body">
        <table id="table-checkout" width="100%"></table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="modalClose()" id="close-modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick="checkOut()">Checkout</button>
      </div>
    </div>
  </div>
</div>
<script>
    $("#booking_checkout").on('submit', function(){
        event.preventDefault();
        if($("#checkin").val() == '')
        {
            swal("Please input!", "Please input check-in date", "warning");
            return;
        }

        if($("#checkout").val() == '')
        {
            swal("Please input!", "Please input check-out date", "warning");
            return;
        }

        if($("#person").val() == 0)
        {
            swal("Please input!", "Please select person", "warning");
            return;
        }

        if($("#child").val() == 0)
        {
            swal("Please input!", "Please select child", "warning");
            return;
        }

        $("#table-checkout").html('');
        $("#table-checkout").append(
            `
            <tr>
                <td>
                    Check-in
                </td>
                <td>
                    ${$("#checkin").val()}
                </td>
            </tr>
            <tr>
                <td>
                    Check-out
                </td>
                <td>
                    ${$("#checkout").val()}
                </td>
            </tr>
            <tr>
                <td>
                    Person
                </td>
                <td>
                    ${$("#person").val()}
                </td>
            </tr>
            <tr>
                <td>
                    Children
                </td>
                <td>
                    ${$("#child").val()}
                </td>
            </tr>
            <tr>
                <td>
                    Room Name
                </td>
                <td>
                    ${$("#room_name").val()}
                </td>
            </tr>
            <tr>
                <td>
                    Total Price
                </td>
                <td>
                    $ ${$("#price").val()}
                </td>
            </tr>`
        );
        
        $("#bookModal").modal("show");
    })

    function checkOut()
    {
        // Check session
        $.ajax({
            url: "models/guestSession.php",
            type: "GET",
            data: { checkSession: 'checkSession' },
            success: function(res)
            {
                let guestSession = JSON.parse(res);
                if(!guestSession == 1)
                {
                    swal("Please login!", "You can't use service reservation", "warning");
                }
                else
                {
                    //////// Start checkout
                    let fromDate = $("#checkin").val();
                    let toDate = $("#checkout").val();
                    let person = $("#person").val();
                    let child = $("#child").val();

                    $.ajax({
                        url: "models/bookingCreate.php",
                        type: "POST",
                        data: { 
                            room_id: <?= $_GET['id'] ?>,
                            from_date: fromDate,
                            to_date: toDate,
                            total_price: <?= $roomModel->getPrice() ?>,
                            person: person,
                            child: child
                        },
                        success: function(res)
                        {
                            swal("Congratulations!", "Your process successfully", "success");
                            setTimeout(function(){
                                window.location.href ="index.php?view=home";
                            }, 2000);
                        }
                    });
                }
            }
        });
    }

    // Close Modal
    function modalClose()
    {
        $("#bookModal").modal("hide");
    }
</script>