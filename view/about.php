<?php
include("models/Room.php");

$roomModel = new Room();

?>
<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
    <div class="ws_images"><ul>
            <li><a href="http://wowslider.net">
                <img src="public/assets/silde/images/bg_login.jpg" alt="responsive slider" title="" id="wows1_0"/></a></li>
            <li>
                <img src="public/assets/silde/images/bg.jpg" alt="" title="" id="wows1_1"/></li>
        </ul></div>
        <div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">css image slider</a> by WOWSlider.com v9.0</div>
    <div class="ws_shadow"></div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div 
            data-aos="fade-down"
            data-aos-delay="10"
            data-aos-duration="2000">
                <h1 class="text-center home-text">
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                &nbsp;&nbsp;
                    Welcome to Star Hotel
                &nbsp;&nbsp;
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                </h1>
                <h3 class="text-center home-text">About Us</h3>
            </div>
        </div>
    </div>
    <!-- Contact -->
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-3">
                    <img class="img-fluid" src="public/assets/images/bg.jpg" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">PHP Tutorial</h5>
                        <br/><br/>
                        <p class="card-text">
                        PHP is a server scripting language, and a powerful tool for making dynamic and interactive Web pages.
                        PHP is a widely-used, free, and efficient alternative to competitors such as Microsoft's ASP.
                        PHP 7 is the latest stable release.
                        </p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>