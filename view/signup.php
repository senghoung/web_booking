<?php
session_start();
if(isset($_SESSION['guest_login']))
{
    echo '<script type="text/javascript">
            location.replace(`index.php?view=home`);
         </script>';
}
?>

<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
    <div class="ws_images"><ul>
            <li><a href="http://wowslider.net">
                <img src="public/assets/silde/images/bg_login.jpg" alt="responsive slider" title="" id="wows1_0"/></a></li>
            <li>
                <img src="public/assets/silde/images/bg.jpg" alt="" title="" id="wows1_1"/></li>
        </ul></div>
        <div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">css image slider</a> by WOWSlider.com v9.0</div>
    <div class="ws_shadow"></div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div 
            data-aos="fade-down"
            data-aos-delay="10"
            data-aos-duration="2000">
                <h1 class="text-center home-text">
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                &nbsp;&nbsp;
                    Welcome to Star Hotel
                &nbsp;&nbsp;
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                </h1>
                <h3 class="text-center home-text">Sign up to start your session!</h3>
            </div>
        </div>
    </div>
    <!-- Contact -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4><i class="fa fa-users" aria-hidden="true"></i> Guest Register</h4></div>
                <div class="card-body">
                    <form id="signup_create" method="POST">
                        <div class="row">
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label>First-name <small class="text-danger">*</small></label>
                                    <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name">
                                </div>
                           </div>
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last-name <small class="text-danger">*</small></label>
                                    <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name">
                                </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email <small class="text-danger">*</small></label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Emaiil">
                                </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                                <div class="form-group">
                                    <label>Phone <small class="text-danger">*</small></label>
                                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone Number">
                                </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                                <div class="form-group">
                                    <label>Password <small class="text-danger">*</small></label>
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                                </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                                <div class="form-group">
                                    <label>Confirm Password <small class="text-danger">*</small></label>
                                    <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm Password">
                                </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-1">
                                <div class="form-group">
                                    <input type="submit" class="form-control btn btn-primary" value="Submit">
                                </div>
                           </div>
                           <div class="col-md-1">
                                <div class="form-group">
                                    <input type="button" onclick="signupReset()" class="form-control btn btn-secondary" id="signup_reset" value="Reset">
                                </div>
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
// Submit Data form User
$("#signup_create").on('submit', function( event ){
    event.preventDefault();

    // Validateion User Submit
    let password = $("#password").val();
    let confirmPassword = $("#confirm_password").val();

    if(confirmPassword != password || password == '')
    {
        swal("Try again!", "Password not match", "error");
        $("#confirm_password").val("");
        $("#password").val("");
        return;
    }

    if(password.length < 6 )
    {
        swal("Try again!", "password required 6 digit!", "error");
        $("#confirm_password").val("");
        $("#password").val("");
        return;
    }

    // Submit Guest Signup 
    $.ajax({
        url: "models/guestSignup.php",
        type: "POST",
        data: $(this).serialize(),
        success: function(res)
        {
            swal("Congratulations!", "Data insert successfully", "success");
            setTimeout(function(){
                window.location.href = "index.php?view=login";
            }, 2000);
        }
    });
});

/////Reset Form
function signupReset()
{
    $("#first_name").val("");
    $("#last_name").val("");
    $("#email").val("");
    $("#phone").val("");
    $("#password").val("");
    $("#confirm_password").val("");
}
</script>