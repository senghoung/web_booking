<?php
include("models/Room.php");

$roomModel = new Room();

?>
<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
    <div class="ws_images"><ul>
            <li><a href="http://wowslider.net">
                <img src="public/assets/silde/images/bg_login.jpg" alt="responsive slider" title="" id="wows1_0"/></a></li>
            <li>
                <img src="public/assets/silde/images/bg.jpg" alt="" title="" id="wows1_1"/></li>
        </ul></div>
        <div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">css image slider</a> by WOWSlider.com v9.0</div>
    <div class="ws_shadow"></div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div 
            data-aos="fade-down"
            data-aos-delay="10"
            data-aos-duration="2000">
                <h1 class="text-center home-text">
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                &nbsp;&nbsp;
                    Welcome to Star Hotel
                &nbsp;&nbsp;
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                </h1>
                <h3 class="text-center home-text">Contact Us</h3>
            </div>
        </div>
    </div>
    <!-- Contact -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4><i class="fa fa-user" aria-hidden="true"></i> Contact Us</h4></div>
                <div class="card-body">
                    <form id="contact">
                        <div class="row">
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label>First-name </label>
                                    <input type="text" class="form-control" placeholder="First Name">
                                </div>
                           </div>
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last-name </label>
                                    <input type="text" class="form-control" placeholder="Last Name">
                                </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email </label>
                                    <input type="email" class="form-control" placeholder="Emaiil">
                                </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                                <div class="form-group">
                                    <label>Phone </label>
                                    <input type="text" class="form-control" placeholder="Phone Number">
                                </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comment </label>
                                    <textarea cols="5" rows="5" class="form-control" placeholder="Comment..."></textarea>
                                </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-1">
                                <div class="form-group">
                                    <input type="submit" class="form-control btn btn-primary" value="Submit">
                                </div>
                           </div>
                           <div class="col-md-1">
                                <div class="form-group">
                                    <input type="button" class="form-control btn btn-secondary" value="Reset">
                                </div>
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>