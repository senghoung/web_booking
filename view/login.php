<?php
session_start();
if(isset($_SESSION['guest_login']))
{
    echo '<script type="text/javascript">
            location.replace(`index.php?view=home`);
         </script>';
}
?>
<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
    <div class="ws_images"><ul>
            <li><a href="http://wowslider.net">
                <img src="public/assets/silde/images/bg_login.jpg" alt="responsive slider" title="" id="wows1_0"/></a></li>
            <li>
                <img src="public/assets/silde/images/bg.jpg" alt="" title="" id="wows1_1"/></li>
        </ul></div>
        <div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">css image slider</a> by WOWSlider.com v9.0</div>
    <div class="ws_shadow"></div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div 
            data-aos="fade-down"
            data-aos-delay="10"
            data-aos-duration="2000">
                <h1 class="text-center home-text">
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                &nbsp;&nbsp;
                    Welcome to Star Hotel
                &nbsp;&nbsp;
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                </h1>
                <h3 class="text-center home-text">Sign in to start your session!</h3>
            </div>
        </div>
    </div>
    <!-- Contact -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4><i class="fa fa-user" aria-hidden="true"></i> Guest Login</h4></div>
                <div class="card-body">
                    <form id="guest_login">
                        <div class="row">
                           <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email <small class="text-danger">*</small></label>
                                    <input type="email" class="form-control" placeholder="Emaiil" name="email" id="email">
                                </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                                <div class="form-group">
                                    <label>Password <small class="text-danger">*</small></label>
                                    <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                                </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-1">
                                <div class="form-group">
                                    <input type="submit" class="form-control btn btn-primary" value="Login">
                                </div>
                           </div>
                           <div class="col-md-1">
                                <div class="form-group">
                                    <input type="button" onclick="loginReset()" class="form-control btn btn-secondary" id="login_reset" value="Reset">
                                </div>
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

// Submit Data form User
$("#guest_login").on('submit', function( event ) {
    event.preventDefault();
    // Submit Guest Login 
    $.ajax({
        url: "models/guestLogin.php",
        type: "POST",
        data: $(this).serialize(),
        success: function(res)
        {
            let messageGuestLogin = JSON.parse(res);

            if(messageGuestLogin == "success")
            {
                swal("Congratulations!", "You start session successfully", "success");
                setTimeout(function()
                { 
                    window.location.reload();
                }, 2000);
            }
            else
            {
                swal("Please try again!", "Your email and password incorrect", "error");
                $("#password").val("");
            }
        }
    });
});

/////Reset Form
function loginReset()
{
    $("#email").val("");
    $("#password").val("");
    $("#email").focus();
}
</script>