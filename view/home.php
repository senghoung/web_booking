<?php
include("models/Room.php");

$roomModel = new Room();

?>
<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
    <div class="ws_images"><ul>
            <li><a href="http://wowslider.net">
                <img src="public/assets/silde/images/bg_login.jpg" alt="responsive slider" title="" id="wows1_0"/></a></li>
            <li>
                <img src="public/assets/silde/images/bg.jpg" alt="" title="" id="wows1_1"/></li>
        </ul></div>
        <div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">css image slider</a> by WOWSlider.com v9.0</div>
    <div class="ws_shadow"></div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div 
            data-aos="fade-down"
            data-aos-delay="10"
            data-aos-duration="1000">
                <h1 class="text-center home-text">
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                &nbsp;&nbsp;
                    Welcome to Star Hotel
                &nbsp;&nbsp;
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                </h1>
                <h3 class="text-center home-text">Booking Now</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card box-booking">
                <div class="card-body">
                    <form id="booking">
                        <div class="row">
                           <div class="col-md-3">
                                <div class="form-group">
                                    <label class="mb-2">Check-in &nbsp;&nbsp;<i class="fa fa-calendar" aria-hidden="true"></i></label>
                                    <input type="date" class="form-control" placeholder="Checkin Date">
                                </div>
                           </div>
                           <div class="col-md-3">
                                <div class="form-group">
                                    <label class="mb-2">Check-out &nbsp;&nbsp;<i class="fa fa-calendar" aria-hidden="true"></i></label>
                                    <input type="date" class="form-control" placeholder="Checkin Date">
                                </div>
                           </div>
                           <div class="col-md-2">
                                <div class="form-group">
                                    <label class="mb-2">Person &nbsp;&nbsp;<i class="fa fa-user" aria-hidden="true"></i></label>
                                    <select class="form-control">
                                        <option value="1">Select Person</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                           </div>
                           <div class="col-md-2">
                                <div class="form-group">
                                    <label class="mb-2">Children &nbsp;&nbsp;<i class="fa fa-child" aria-hidden="true"></i></label>
                                    <select class="form-control">
                                        <option value="1">Select Child</option>
                                        <option value="1">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                           </div>
                           <div class="col-md-2">
                                <div class="form-group">
                                    <label class="mb-4"></label>
                                    <input type="submit" class="form-control btn btn-primary" value="BOOK NOW">
                                </div>
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Room list -->
    <div class="row">
    <div class="col-md-12 col-xs-12">
        <div data-aos="fade-up" data-aos-delay="4"
        data-aos-duration="1000">
            <!-- <div class="card-header">ROOM LIST</div> -->
            <div class="row">
                <?php foreach ($roomModel->getRoomAll() as $key => $value) { ?>
                    <div class="col-md-3">
                        <div class="card-room-list img-thumbnail" style="width: 18rem">
                            <img src="public/assets/images/uploads/<?= $value['photo'] ?>" width="200px" height="180px" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title"><?= $value['name'] ?> &nbsp;&nbsp; $<?= $value['price'] ?></h5>
                                <p class="card-text"><?= $value['description'] ?></p>
                                <a href="#" onclick="bookingDetail(<?= $value['id'] ?>)" class="btn btn-primary">Book Now</a>
                            </div>
                        </div>
                    </div>  
                <?php  } ?>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    // Submit Data form book
    $("#booking").on('submit', function( event ) {
        event.preventDefault();
        window.location.href = "index.php?view=room";
    });
</script>