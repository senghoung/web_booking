<?php
include("models/Room.php");

$roomModel = new Room();

?>
<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
    <div class="ws_images"><ul>
            <li><a href="http://wowslider.net">
                <img src="public/assets/silde/images/bg_login.jpg" alt="responsive slider" title="" id="wows1_0"/></a></li>
            <li>
                <img src="public/assets/silde/images/bg.jpg" alt="" title="" id="wows1_1"/></li>
        </ul></div>
        <div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">css image slider</a> by WOWSlider.com v9.0</div>
    <div class="ws_shadow"></div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div 
            data-aos="fade-down"
            data-aos-delay="10"
            data-aos-duration="1000">
                <h1 class="text-center home-text">
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                &nbsp;&nbsp;
                    Welcome to Star Hotel
                &nbsp;&nbsp;
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-star" style="color: orange;font-size:30px;" aria-hidden="true"></i>
                </h1>
                <h3 class="text-center home-text">Rooms List</h3>
            </div>
        </div>
    </div>
    <!-- Room list -->
    <div class="row">
        <div class="col-md-12">
            <div data-aos="fade-up" data-aos-delay="4"
            data-aos-duration="1000">
                <!-- <div class="card-header">ROOM LIST</div> -->
                <div class="row">
                    <?php foreach ($roomModel->getRoomAll() as $key => $value) { ?>
                        <div class="col-md-3">
                            <div class="card-room-list img-thumbnail" style="width: 18rem">
                                <img src="public/assets/images/uploads/<?= $value['photo'] ?>" width="200px" height="180px" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title"><?= $value['name'] ?> &nbsp;&nbsp; $<?= $value['price'] ?></h5>
                                    <p class="card-text"><?= $value['description'] ?></p>
                                    <a href="#" onclick="bookingDetail(<?= $value['id'] ?>)" class="btn btn-primary">Book Now</a>
                                </div>
                            </div>
                        </div>  
                    <?php  } ?>
                </div>
            </div>
        </div>
    </div>
</div>