<?php
    include("../models/Role.php");

    $currentDateTime = date('Y-m-d H:i:s');
    $roleModel = new Role();
?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <br/>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-0">Create Floor</h4>
                        </div>
                    </div>    
                    </div>
                    <div class="card-body">
                        <form id="floor_create" method="POST">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name <small class="text-danger">*</small></label>
                                        <input type="text" name="name" id="name" placeholder="Enter Name" class="form-control" require>
                                        <small id="name_validate" class="text-danger"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Description </label>
                                        <input type="text" name="description" id="description" placeholder="Enter Description" class="form-control">
                                    </div>
                                    <div hidden class="form-group">
                                        <label>Create At<small class="text-danger">*</small></label>
                                        <input type="date" name="created_at" id="created_at" class="form-control" value="2021-01-31 20:31:57" require>
                                    </div>
                                    <div hidden class="form-group">
                                        <label>Create By <small class="text-danger">*</small></label>
                                        <input type="password" name="created_by" id="created_by" class="form-control" value="1" require>
                                    </div>
                                    <div hidden class="form-group">
                                        <label>Updatd At <small class="text-danger">*</small></label>
                                        <input type="date" name="updated_at" id="updated_at" class="form-control" value="2021-01-31 20:31:57" require>
                                    </div>
                                    <div hidden class="form-group">
                                        <label>Update By <small class="text-danger">*</small></label>
                                        <input type="password" name="updated_by" id="updated_by" class="form-control" value="1" require>
                                    </div>

                                    <div class="form-group">
                                        <a href="index.php?view=floor_list" class="btn btn-danger float-right">Cancel</a>
                                        <input type="submit" value="Create" class="btn btn-success float-right mr-2">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>

    // Submit Data form User
    $("#floor_create").on('submit', function( event ) {
        event.preventDefault();
        
        // Validateion Role Submit
        let mesageError = String;
        let name = $("#name").val();

        if(name == "" || name == undefined )
        {
            mesageError = "name required!";
            $("#name_validate").text(mesageError);
            return;
        }
        
        $.ajax({
            url: "../models/floorCreate.php",
            type: "POST",
            data: $(this).serialize(),
            success: function(res)
            {
                swal("Congratulations!", "Data insert successfully", "success");
                setTimeout(function(){
                    window.location.href = "index.php?view=floor_list";
                }, 2000);
            }
        });
    });
    </script>