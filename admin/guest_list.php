<?php
    include("../models/Guest.php");
    $guestModel = new Guest();
    ?>

    <!-- Content Wrapper. Contains page content -->
    <script>
    $(document).ready( function (e) {
        $('#table_id').DataTable();
    });
    </script>
    <script>
        function edit_guest(id)
        {
            window.location.href = "index.php?view=guest_edit&id=" + id;
        }

        function delete_guest(id)
        {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: false
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Poof! Your imaginary file has been deleted!", { icon: "success" });
                    $.ajax({
                    url: "../models/guestDelete.php",
                    type: "GET",
                    data: { id: id },
                    contentType: "application/json",
                    dataType: "json",
                    success: function(res)
                    {
                        setTimeout(function(){
                            location.reload();
                        }, 2000);
                    }
                    });
                }
            });
        }
    </script>
    <div class="content-wrapper">
        <!-- Main content -->
        <br/>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-0">List Guest</h4>
                        </div>
                        <div class="col-md-6">
                            <a type="button" href="index.php?view=guest_create" class="btn btn-success float-right"><i class="nav-icon fas fa-plus"></i> New</a>
                        </div>
                    </div>    
                    </div>
                    <div class="card-body">
                    <table id="table_id" class="table display">
                        <thead class="thead-light">
                            <tr>
                                <th hidden>ID</th>
                                <th>No</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($guestModel->getGuestAll() as $key => $value) { ?>
                            <tr>
                                <td hidden><?= $value['id'] ?></td>
                                <td><?= $key + 1 ?></td>
                                <td><?= $value['first_name'] ?></td>
                                <td><?= $value['last_name'] ?></td>
                                <td><?= $value['email'] ?></td>
                                <td><?= $value['phone'] ?></td>
                                <td>
                                    <a onclick="edit_guest(<?= $value['id']?>)" class="btn btn-success btn-sm" href="javascript:void(0)"> Edit<i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a onclick="delete_guest(<?= $value['id']?>)" class="btn btn-danger btn-sm" href="javascript:void(0)"> Delete</a>
                                </td>
                            </tr>
                            <?php  } ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->