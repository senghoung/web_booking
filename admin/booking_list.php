<?php
    include("../models/Booking.php");
    $bookingModel = new Booking();
    ?>

    <!-- Content Wrapper. Contains page content -->
    <script>
    $(document).ready( function (e) {
        $('#table_id').DataTable();
    });
    </script>
    <script>
        function edit_floor(id)
        {
            window.location.href = "index.php?view=floor_edit&id=" + id;
        }

        function delete_floor(id)
        {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: false
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Poof! Your imaginary file has been deleted!", { icon: "success" });
                    $.ajax({
                    url: "../models/floorDelete.php",
                    type: "GET",
                    data: { id: id },
                    contentType: "application/json",
                    dataType: "json",
                    success: function(res)
                    {
                        setTimeout(function(){
                            location.reload();
                        }, 2000);
                    }
                    });
                }
            });
        }
    </script>
    <div class="content-wrapper">
        <!-- Main content -->
        <br/>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-0">List Booking</h4>
                        </div>
                        <!-- <div class="col-md-6">
                            <a type="button" href="index.php?view=floor_create" class="btn btn-success float-right"><i class="nav-icon fas fa-plus"></i> New</a>
                        </div> -->
                    </div>    
                    </div>
                    <div class="card-body">
                    <table id="table_id" class="table display">
                        <thead class="thead-light">
                            <tr>
                                <th hidden>ID</th>
                                <th>No</th>
                                <th>Guest</th>
                                <th>Room</th>
                                <th>Check-in</th>
                                <th>Check-out</th>
                                <th>Person</th>
                                <th>Child</th>
                                <th>Total Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($bookingModel->getBookingAll() as $key => $value) { ?>
                            <tr>
                                <td hidden><?= $value['id'] ?></td>
                                <td><?= $key + 1 ?></td>
                                <td><?= $value['guest_name'] ?></td>
                                <td><?= $value['room_name'] ?></td>
                                <td><?= $value['from_date'] ?></td>
                                <td><?= $value['to_date'] ?></td>
                                <td><?= $value['person'] ?></td>
                                <td><?= $value['child'] ?></td>
                                <td>$ <?= $value['total_price'] ?></td>
                                <!-- <td>
                                    <a onclick="edit_floor(<?= $value['id']?>)" class="btn btn-success btn-sm" href="javascript:void(0)"> Edit<i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a onclick="delete_floor(<?= $value['id']?>)" class="btn btn-danger btn-sm" href="javascript:void(0)"> Delete</a>
                                </td> -->
                            </tr>
                            <?php  } ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->