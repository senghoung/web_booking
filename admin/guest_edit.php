<?php
    include("../models/Guest.php");

    $guestModel = new Guest();
    $guestModel->setFirstName($guestModel->getGuestById($_GET['id'])['first_name']);
    $guestModel->setLastName($guestModel->getGuestById($_GET['id'])['last_name']);
    $guestModel->setPhone($guestModel->getGuestById($_GET['id'])['phone']);
    $guestModel->setEmail($guestModel->getGuestById($_GET['id'])['email']);
?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <br/>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-0">Update Guest</h4>
                        </div>
                    </div>    
                    </div>
                    <div class="card-body">
                        <form id="guest_create" method="POST">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>First Name <small class="text-danger">*</small></label>
                                        <input type="hidden" value="<?= $_GET['id'] ?>" id="guest_id" name="guest_id">
                                        <input type="text" name="first_name" id="first_name" placeholder="Enter FirstName" value="<?= $guestModel->getFirstName() ?>" class="form-control" require>
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name <small class="text-danger">*</small></label>
                                        <input type="text" name="last_name" id="last_name" value="<?= $guestModel->getLastName() ?>" placeholder="Enter LastName" class="form-control" require>
                                    </div>
                                    <div class="form-group">
                                        <label>Email <small class="text-danger">*</small></label>
                                        <input type="email" name="email" id="email" value="<?= $guestModel->getEmail() ?>" placeholder="Enter Email" class="form-control" require>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone <small class="text-danger"></small></label>
                                        <input type="text" name="phone" id="phone" value="<?= $guestModel->getPhone() ?>" placeholder="Enter Phone" class="form-control" require>
                                    </div>
                                    <div class="form-group">
                                        <label>Password <small class="text-danger">*</small></label>
                                        <input type="password" name="password" id="password" placeholder="Enter Password" class="form-control" require>
                                    </div>
                                    <div class="form-group">
                                        <a href="index.php?view=guest_list" class="btn btn-danger float-right">Cancel</a>
                                        <input type="submit" value="Update" class="btn btn-success float-right mr-2">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>

    // Submit Data form User
    $("#guest_create").on('submit', function( event ) {
        event.preventDefault();
        
        $.ajax({
            url: "../models/guestUpdate.php",
            type: "POST",
            data: $(this).serialize(),
            success: function(res)
            {
                swal("Congratulations!", "Data insert successfully", "success");
                setTimeout(function(){
                    window.location.href = "index.php?view=guest_list";
                }, 2000);
            }
        });
    });
    </script>