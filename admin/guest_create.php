<?php
    include("../models/Role.php");

    $currentDateTime = date('Y-m-d H:i:s');
    $roleModel = new Role();
?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <br/>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-0">Create Guest</h4>
                        </div>
                    </div>    
                    </div>
                    <div class="card-body">
                        <form id="guest_create" method="POST">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>First Name <small class="text-danger">*</small></label>
                                        <input type="text" name="first_name" id="first_name" placeholder="Enter FirstName" class="form-control" require>
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name <small class="text-danger">*</small></label>
                                        <input type="text" name="last_name" id="last_name" placeholder="Enter LastName" class="form-control" require>
                                    </div>
                                    <div class="form-group">
                                        <label>Email <small class="text-danger">*</small></label>
                                        <input type="email" name="email" id="email" placeholder="Enter Email" class="form-control" require>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone <small class="text-danger"></small></label>
                                        <input type="text" name="phone" id="phone" placeholder="Enter Phone" class="form-control" require>
                                    </div>
                                    <div class="form-group">
                                        <label>Password <small class="text-danger">*</small></label>
                                        <input type="password" name="password" id="password" placeholder="Enter Password" class="form-control" require>
                                    </div>
                                    <div class="form-group">
                                        <a href="index.php?view=guest_list" class="btn btn-danger float-right">Cancel</a>
                                        <input type="submit" value="Create" class="btn btn-success float-right mr-2">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>

    // Submit Data form User
    $("#guest_create").on('submit', function( event ) {
        event.preventDefault();
        
        $.ajax({
            url: "../models/guestCreate.php",
            type: "POST",
            data: $(this).serialize(),
            success: function(res)
            {
                swal("Congratulations!", "Data insert successfully", "success");
                setTimeout(function(){
                    window.location.href = "index.php?view=guest_list";
                }, 2000);
            }
        });
    });
    </script>