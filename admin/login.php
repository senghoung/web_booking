<?php 
session_start(); 
if(isset($_SESSION['user_login']))
{
  echo '<script type="text/javascript">
            location.replace(`${window.origin}/web_cita/admin`);
        </script>';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Cita</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="../public/assets/plugins/fontawesome-free/css/all.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="../public/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../public/assets/dist/css/adminlte.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
    <!-- jQuery -->
    <script src="../public/assets/plugins/jquery/jquery.min.js"></script>

    <style>
        body {
        background-image: url("../public/assets/images/bg_login.jpg");
        background-color: #cccccc;
        background-repeat: no-repeat;
        background-size: cover;
        background-attachment: fixed;
        background-position: center;
      }
      .login-card-body, .register-card-body {
        background-color: #fff;
        border-top: 0;
        color: #666;
        padding: 20px;
        height: 300px;
        border-radius: 50px;
    }
    .btn-primary{
        background-color: #64cedb;
        border-color: #64cedb;
        box-shadow: 0px 2px 2px 0px #198754;
    }
    
    .btn-primary:hover{
        background-color: #37bcce;
        border-color: #64cedb;
    
    }

    /* Paste this css to your style sheet file or under head tag */
    /* This only works with JavaScript, 
    if it's not present, don't show loader */
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url("../public/assets/images/Preloader_11.gif") center no-repeat #fff;
    }
    </style>
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>

<script>
    //paste this code under the head tag or in a separate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeToggle("slow");
	});
</script>

<body class="hold-transition login-page">
<div class="se-pre-con"></div>
<div class="login-box">
  <div class="login-logo">
    <!-- <a href="../../index2.html"><b>Admin</b> BOOKING</a> -->
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form id="user_login" method="post">
        <div class="input-group mb-3">
          <input type="email" class="form-control" id="email" name="email" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Login</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<script>

// Submit Data form User
$("#user_login").on('submit', function( event ) {
    event.preventDefault();
    
    $.ajax({
        url: "../models/userLogin.php",
        type: "POST",
        data: $(this).serialize(),
        success: function(res)
        {
          let userLogin = JSON.parse(res);
          console.log(userLogin);
          // return;

          if(userLogin == "success")
          {
            swal("Congratulations!", "You start session successfully", "success");

            setTimeout(function()
            { 
              window.location.reload();
            }, 2000);
          }
          else
          {
            swal("Please try again!", "Your email and password incorrect", "error");
            $("#password").val("");
          }
        }
    });
});
</script>

<!-- REQUIRED SCRIPTS -->
<!-- Bootstrap -->
<script src="../public/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="../public/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../public/assets/dist/js/adminlte.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="../public/assets/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="../public/assets/plugins/raphael/raphael.min.js"></script>
<script src="../public/assets/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="../public/assets/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="../public/assets/plugins/chart.js/Chart.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="../public/assets/dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../public/assets/dist/js/pages/dashboard2.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
</body>
</html>