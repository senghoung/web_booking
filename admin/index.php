    <?php include_once("../models/BaseModel.php") ?>
    <?php include("../Config.php") ?>
    <?php
        session_start();
        $viwe = 'dashboard';
        
        if(isset($_SESSION["user_login"])) {
            include("../admin/includes/header.php");
            include("../admin/includes/sidebar.php");

            if(isset($_GET['view']))
            {
                $viwe = $_GET['view'];
            }

            include("../admin/$viwe.php");
        }
        else
        {
            echo '<script type="text/javascript">
                    location.replace(`${window.origin}/web_cita/admin/login.php`);
                </script>';
        }

        include("../admin/includes/footer.php");
    ?>