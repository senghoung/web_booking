<?php
    include("../models/Room.php");

    $roomModel = new Room();

    ?>

    <!-- Content Wrapper. Contains page content -->
    <script>
    $(document).ready( function (e) {
        $('#table_id').DataTable();
    });
    </script>
    <script>
        function edit_room(id)
        {
            window.location.href = "index.php?view=room_edit&id=" + id;
        }

        function delete_room(id)
        {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: false
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Poof! Your imaginary file has been deleted!", { icon: "success" });
                    $.ajax({
                    url: "../models/roomDelete.php",
                    type: "GET",
                    data: { id: id },
                    contentType: "application/json",
                    dataType: "json",
                    success: function(res)
                    {
                        setTimeout(function(){
                            location.reload();
                        }, 2000);
                    }
                    });
                }
            });
        }
    </script>
    <div class="content-wrapper">
        <!-- Main content -->
        <br/>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-0">List Room</h4>
                        </div>
                        <div class="col-md-6">
                            <a type="button" href="index.php?view=room_create" class="btn btn-success float-right"><i class="nav-icon fas fa-plus"></i> New</a>
                        </div>
                    </div>    
                    </div>
                    <div class="card-body">
                    <table id="table_id" class="table display">
                        <thead class="thead-light">
                            <tr>
                                <th hidden>ID</th>
                                <th>No</th>
                                <!-- <th>Number</th> -->
                                <th>Name</th>
                                <th>Image</th>
                                <th>Price</th>
                                <th>Recorder</th>
                                <th>Record Date</th>
                                <th>Modyfier</th>
                                <th>Modyfied Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($roomModel->getRoomAll() as $key => $value) { ?>
                            <tr>
                                <td hidden><?= $value['id'] ?></td>
                                <td><?= $key + 1 ?></td>
                                <!-- <td><?= $value['room_number'] ?></td> -->
                                <td><?= $value['name'] ?></td>
                                <td><img class="rounded" width="40px" height="40px" src="../public/assets/images/uploads/<?= $value['photo'] ?>" /></td>
                                <td>$ <?= $value['price'] ?></td>
                                <td><?= $value['createdby'] ?></td>
                                <td><?= $value['created_at'] ?></td>
                                <td><?= $value['updatedby'] ?></td>
                                <td><?= $value['updated_at'] ?></td>
                                <td>
                                    <a onclick="edit_room(<?= $value['id']?>)" class="btn btn-success btn-sm" href="javascript:void(0)"> Edit<i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a onclick="delete_room(<?= $value['id']?>)" class="btn btn-danger btn-sm" href="javascript:void(0)"> Delete</a>
                                </td>
                            </tr>
                            <?php  } ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->