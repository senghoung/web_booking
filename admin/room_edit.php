<?php
    include("../models/Room.php");
    include("../models/Floor.php");

    $roomModel = new Room();
    $floorModel = new Floor();
    $roomModel->setPhoto($roomModel->getRoomById($_GET['id'])['photo']);
    $roomModel->setName($roomModel->getRoomById($_GET['id'])['name']);
    $roomModel->setRoomNumber($roomModel->getRoomById($_GET['id'])['room_number']);
    $roomModel->setFloorId($roomModel->getRoomById($_GET['id'])['floor_id']);
    $roomModel->setPrice($roomModel->getRoomById($_GET['id'])['price']);
    $roomModel->setDescription($roomModel->getRoomById($_GET['id'])['description']);
?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <br/>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-0">Update Room</h4>
                        </div>
                    </div>    
                    </div>
                    <div class="card-body">
                        <form id="room_edit" method="POST" enctype='multipart/form-data'>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Floor <small class="text-danger">*</small></label>
                                                <select id="floor" name="floor" class="form-control">
                                                <option>Select Floor</option>
                                                <?php foreach ($floorModel->getFloorAll() as $key => $value) { ?>
                                                    <option <?php if($roomModel->getFloorId() == $value['id']) echo "selected" ?> value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                                <?php } ?>
                                                </select>
                                                <small id="name_validate" class="text-danger"></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <input type="hidden" name="room_id" id="room_id" value="<?= $_GET['id'] ?>" class="form-control">
                                                <label>Name <small class="text-danger">*</small></label>
                                                <input type="text" name="name" id="name" placeholder="Enter Name" value="<?= $roomModel->getName() ?>" class="form-control" require>
                                                <small id="name_validate" class="text-danger"></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Number <small class="text-danger">*</small></label>
                                                <input type="text" name="room_number" id="room_number" placeholder="Enter Number" value="<?= $roomModel->getRoomNumber() ?>" class="form-control" require>
                                                <small id="room_number_validate" class="text-danger"></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Price <small class="text-danger">*</small></label>
                                                <input type="number" name="price" id="price" placeholder="Enter Price" class="form-control" value="<?= $roomModel->getPrice() ?>" require>
                                                <small id="price_number_validate" class="text-danger"></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Description </label>
                                                <input type="text" name="description" id="description" placeholder="Enter Description" value="<?= $roomModel->getDescription() ?>" class="form-control" require>
                                            </div>
                                        </div>
                                    </div>
                                    <div hidden class="form-group">
                                        <label>Create At </label>
                                        <input type="date" name="created_at" id="created_at" class="form-control" value="2021-01-31 20:31:57" require>
                                    </div>
                                    <div hidden class="form-group">
                                        <label>Create By </label>
                                        <input type="password" name="created_by" id="created_by" class="form-control" value="1" require>
                                    </div>
                                    <div hidden class="form-group">
                                        <label>Updatd At </label>
                                        <input type="date" name="updated_at" id="updated_at" class="form-control" value="2021-01-31 20:31:57" require>
                                    </div>
                                    <div hidden class="form-group">
                                        <label>Update By </label>
                                        <input type="password" name="updated_by" id="updated_by" class="form-control" value="1" require>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input hidden type="text" id="photo" name="photo">
                                            <div class="form-group">
                                            <label>Image </label>
                                                <div class="card">
                                                    <img class="img-fluid img-thumbnai" src="../public/assets/images/uploads/<?=$roomModel->getPhoto() ?>" width="450px" height="400px" name="image_preview" id="image_preview" src="../public/assets/images/no_image.jpg" alt="image" />
                                                </div>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Upload</span>
                                                    </div>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="fileUpload" name="fileUpload" value="<?=$roomModel->getPhoto()?>" onchange="readURL(this)">
                                                        <label class="custom-file-label" for="photo">Choose image</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                    <a href="index.php?view=room_list" class="btn btn-danger float-right">Cancel</a>
                                    <input type="submit" value="Update" class="btn btn-success float-right mr-2">
                                </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_preview').attr('src', e.target.result);
                    $("#photo").val($('input[type=file]').val());
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <script>

    // Submit Data form User
    $("#room_edit").on('submit', function( event ) {
        event.preventDefault();

        let fileUpload = $('#fileUpload').prop('files')[0];
        let formData = new FormData(this);                  
        formData.append('fileUpload', fileUpload);
        
        let roomObject =  {
            fileUpload: fileUpload
        }

        $.ajax({
            url: "../models/roomUpdate.php",
            type: "POST",
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function(res)
            {
                swal("Congratulations!", "Data insert successfully", "success");
                setTimeout(function(){
                    window.location.href = "index.php?view=room_list";
                }, 2000);
                
                // console.log(res);
            }
        });
    });
    </script>