<?php
    include("../models/Role.php");
    include("../models/User.php");

    $currentDateTime = date('Y-m-d H:i:s');
    $roleModel = new Role();
    $userModel = new User();
    $userModel->setUsername($userModel->getUserById($_GET['id'])['username']);
    $userModel->setEmail($userModel->getUserById($_GET['id'])['email']);
    $userModel->setPassword($userModel->getUserById($_GET['id'])['password']);
    $userModel->setRole($userModel->getUserById($_GET['id'])['role_id']);
?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <br/>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-0">Update User</h4>
                        </div>
                    </div>    
                    </div>
                    <div class="card-body">
                        <form id="user_update" method="POST">
                            <div class="row">
                                <div class="col-md-12">
                                    <input hidden type="text" name="user_id" id="user_id" value="<?=  $_GET['id'] ?>">
                                    <div class="form-group">
                                        <label>Username <small class="text-danger">*</small></label>
                                        <input type="text" name="username" id="username" placeholder="Enter Username" class="form-control" value="<?= $userModel->getUsername() ?>" require>
                                        <small id="username_validate" class="text-danger"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Email <small class="text-danger">*</small></label>
                                        <input type="email" name="email" id="email" placeholder="Enter Email" class="form-control" value="<?= $userModel->getEmail() ?>" require>
                                        <small id="email_validate" class="text-danger"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Password <small class="text-danger">*</small></label>
                                        <input type="password" name="password" id="password" placeholder="Enter Password" class="form-control">
                                        <small id="password_validate" class="text-danger"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Role <small class="text-danger">*</small></label>
                                        <select name="role_id" id="role_id" class="form-control" require>
                                            <option value="0">Select Role</option>
                                            <?php  foreach ($roleModel->getRoleAll() as $key => $value) { ?>
                                                <option <?php if($userModel->getRole() == $value['id']) echo "selected" ?> value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                            <?php } ?>
                                        </select>
                                        <small id="role_validate" class="text-danger"></small>
                                    </div>
                                    <div hidden class="form-group">
                                        <label>Create At<small class="text-danger">*</small></label>
                                        <input type="datetime" name="created_at" id="created_at" class="form-control" value="<?= $currentDateTime  ?>" require>
                                    </div>
                                    <div hidden class="form-group">
                                        <label>Create By <small class="text-danger">*</small></label>
                                        <input type="password" name="created_by" id="created_by" class="form-control" value="1" require>
                                    </div>
                                    <div hidden class="form-group">
                                        <label>Updatd At <small class="text-danger">*</small></label>
                                        <input type="datetime" name="updated_at" id="updated_at" class="form-control" value="<?= $currentDateTime  ?>" require>
                                    </div>
                                    <div hidden class="form-group">
                                        <label>Update By <small class="text-danger">*</small></label>
                                        <input type="password" name="updated_by" id="updated_by" class="form-control" value="1" require>
                                    </div>

                                    <div class="form-group">
                                        <a href="index.php?view=user_list" class="btn btn-danger float-right">Cancel</a>
                                        <input type="submit" value="Update" class="btn btn-success float-right mr-2">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>

    // Submit Data form User
    $("#user_update").on('submit', function( event ) {
        event.preventDefault();
        
        // Validateion User Submit
        let mesageError = String;
        let username = $("#username").val();
        let email = $("#email").val();
        let role = $("#role_id").val();

        if(username == "" || username == undefined )
        {
            mesageError = "username required!";
            $("#username_validate").text(mesageError);
            return;
        }

        if(email == "" || email == undefined )
        {
            mesageError = "email required!";
            $("#email_validate").text(mesageError);
            return;
        }

        if(password.length < 6 )
        {
            mesageError = "password required 6 digit!";
            $("#password_validate").text(mesageError);
            return;
        }

        if(role == 0 || role == undefined )
        {
            mesageError = "role required!";
            $("#role_validate").text(mesageError);
            return;
        }
        
        $.ajax({
            url: "../models/userUpdate.php",
            type: "POST",
            data: $(this).serialize(),
            // dataType: "json",
            success: function(res)
            {
                swal("Congratulations!", "Data update successfully", "success");
                setTimeout(function(){
                    window.location.href = "index.php?view=user_list";
                }, 2000);
                // console.log(res);
            }
        });
    });
    </script>