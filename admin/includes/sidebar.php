
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-warning navbar-badge">15</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">15 Notifications</span>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-envelope mr-2"></i> 4 new messages
                        <span class="float-right text-muted text-sm">3 mins</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-users mr-2"></i> 8 friend requests
                        <span class="float-right text-muted text-sm">12 hours</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-file mr-2"></i> 3 new reports
                        <span class="float-right text-muted text-sm">2 days</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                </div>
            </li>

            <!-- Messages Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i>
                        <img style="position: relative; width: 40px; top: -8px;" src="../public/assets/images/profile.png"/>
                    </i>
                    <!-- <span class="badge badge-danger navbar-badge">3</span> -->
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <a href="index.php?view=my_account" class="dropdown-item">
                        <!-- Message End -->
                        <i class="fas fa-user mr-2"></i> My Account
                        <span class="float-right text-muted text-sm"></span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a onclick="logOut()" href="#" class="dropdown-item">
                        <!-- Message End -->
                        <i class="fas fa-lock mr-2"></i>Logout
                        <span class="float-right text-muted text-sm"></span>
                    </a>
                </div>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

     <!-- Main Sidebar Container -->
     <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="index.php?view=dashboard" class="brand-link">
            <img src="../public/assets/images/logo.png" alt="AdminLTE Logo" class="brand-image" style="opacity:0.8">
            <!-- <span class="brand-text font-weight-light">Admin Cita</span> -->
        </a>
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Admins
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="index.php?view=role_list" class="nav-link">
                                    <!-- <i class="far fa-circle nav-icon"></i> -->
                                    <p>Role</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="index.php?view=user_list" class="nav-link">
                                    <!-- <i class="far fa-circle nav-icon"></i> -->
                                    <p>User</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-bed"></i>
                            <p>
                                Rooms
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="index.php?view=floor_list" class="nav-link">
                                    <!-- <i class="far fa-circle nav-icon"></i> -->
                                    <p>Floor</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="index.php?view=room_list" class="nav-link">
                                    <!-- <i class="far fa-circle nav-icon"></i> -->
                                    <p>Room</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <i class="fa fa-address-book-o" aria-hidden="true"></i>
                            <p>
                                Reservation
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                        <li class="nav-item">
                                <a href="index.php?view=guest_list" class="nav-link">
                                    <!-- <i class="far fa-circle nav-icon"></i> -->
                                    <p>Guest</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="index.php?view=booking_list" class="nav-link">
                                    <!-- <i class="far fa-circle nav-icon"></i> -->
                                    <p>Booking</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="index.php?view=booking_report" class="nav-link">
                            <i class="nav-icon far fa-copy"></i>
                            <p>Report Booking</p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <script type="text/javascript">
        function logOut()
        {
            $.ajax({
                url: "../models/userLogin.php",
                type: "GET",
                data: { logout: 'logout' },
                success: function(res)
                {
                    if(res = "logout")
                    {
                        window.location.reload();
                    }
                }
            });
        }
    </script>