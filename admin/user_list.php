    <?php
    include("../models/User.php");
    $userModel = new User();
    ?>

    <!-- Content Wrapper. Contains page content -->
    <script>
    $(document).ready( function (e) {
        $('#table_id').DataTable();
    });
    </script>
    <script>
        function edit_uer(id)
        {
            window.location.href = "index.php?view=user_edit&id=" + id;
        }

        function delete_user(id)
        {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: false
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Poof! Your imaginary file has been deleted!", { icon: "success" });
                    $.ajax({
                    url: "../models/userDelete.php",
                    type: "GET",
                    data: { id: id },
                    contentType: "application/json",
                    dataType: "json",
                    success: function(res)
                    {
                        setTimeout(function(){
                            location.reload();
                        }, 2000);
                    }
                    });
                }
            });
        }
    </script>
    <div class="content-wrapper">
        <!-- Main content -->
        <br/>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-0">List Users</h4>
                        </div>
                        <div class="col-md-6">
                            <a type="button" href="index.php?view=user_create" class="btn btn-success float-right"><i class="nav-icon fas fa-plus"></i> New</a>
                        </div>
                    </div>    
                    </div>
                    <div class="card-body">
                    <table id="table_id" class="table display">
                        <thead class="thead-light">
                            <tr>
                                <th hidden>ID</th>
                                <th>No</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Recorder</th>
                                <th>Record Date</th>
                                <th>Modyfier</th>
                                <th>Modyfied Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($userModel->getUserAll() as $key => $value) { ?>
                            <tr>
                                <td hidden><?= $value['id'] ?></td>
                                <td><?= $key + 1 ?></td>
                                <td><?= $value['username'] ?></td>
                                <td><?= $value['email'] ?></td>
                                <td><?= $value['role_name'] ?></td>
                                <td><?= $value['createdby'] ?></td>
                                <td><?= $value['created_at'] ?></td>
                                <td><?= $value['updatedby'] ?></td>
                                <td><?= $value['updated_at'] ?></td>
                                <td>
                                    <a onclick="edit_uer(<?= $value['id']?>)" class="btn btn-success btn-sm" href="javascript:void(0)"> Edit<i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a onclick="delete_user(<?= $value['id']?>)" class="btn btn-danger btn-sm" href="javascript:void(0)"> Delete</a>
                                </td>
                            </tr>
                            <?php  } ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->